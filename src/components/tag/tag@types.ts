export interface IProps {
  name: string
  isActive?: boolean
  isUsed?: boolean
}