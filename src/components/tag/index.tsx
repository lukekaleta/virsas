import { IProps } from './tag@types'

import './styles.scss'
import { useState } from 'react'

function Tag(props: IProps) {
  // state
  const [active, setActive] = useState<boolean>(false)

  // props
  const { name, isActive = false, isUsed = false } = props

  // classes
  const isActiveClass = active ? 'tag--active' : ''
  const isUsedClass = isUsed ? 'tag--disabled' : ''

  // tag actions
  const tagAction = () => {
    return setActive(!active)
  }

  return (
    <div onClick={tagAction} className={`tag ${isActiveClass} ${isUsedClass}`}>
      {name}
    </div>
  )
}

export default Tag
