import { IProps } from './header@types'
import './styles.scss'

function Header(props: IProps) {
  const { name } = props

  return <div className='header'>{name}</div>
}

export default Header
