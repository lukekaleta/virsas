export interface IProps {
  name: string
  onClick?: () => void
  type?: 'primary' | 'secondary'
}
