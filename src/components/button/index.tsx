import { IProps } from './button@types'
import './styles.scss'

function Button(props: IProps) {
  const { name, onClick, type = 'primary' } = props

  const typeClass = `button--${type}`

  const emptyAction = () => {
    console.warn('On click is not set!')
  }

  return (
    <button
      type='button'
      className={`button ${typeClass}`}
      onClick={onClick || emptyAction}
    >
      {name}
    </button>
  )
}

export default Button
