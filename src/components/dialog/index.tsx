import Header from '../header'
import './styles.scss'
import remote from '../../remote/data.json'
import Tag from '../tag'
import Button from '../button'

interface IDialogProps {
  clickAction?: () => void
}

const Dialog = (props: IDialogProps) => {
  const { clickAction } = props

  return (
    <div className='dialog-background'>
      <div className='dialog'>
        <div className='dialog-header'>
          <div>
            <Header name='Add new list' />
          </div>
        </div>
        <div className='dialog-content'>
          <div>
            {remote.data.map((d) => (
              <Tag key={d} name={d} />
            ))}
          </div>
          <div>
            {remote.data.map((d) => (
              <Tag key={d} name={d} />
            ))}
          </div>
        </div>

        <div className='dialog-bottom'>
          <Button onClick={clickAction} type='secondary' name='Close' />
          <Button onClick={clickAction} name='Save' />
        </div>
      </div>
    </div>
  )
}

export default Dialog
