import { useState } from 'react'
import Button from '../button'
import Dialog from '../dialog'
import Tag from '../tag'
import './styles.scss'

const Box = () => {
  // state
  const [dialog, setDialog] = useState<boolean>(false)

  // dialog actions
  const openDialog = () => {
    setDialog(true)
  }

  const closeDialog = () => {
    setDialog(false)
  }

  // TODO: Rework
  const sampleData = ['C3', 'C4']

  return (
    <div className='box'>
      <div className='box-content'>
        {sampleData.map((j) => (
          <>{j}</>
        ))}
      </div>
      <div className='box-content'>
        {sampleData.map((j) => (
          <>{j}</>
        ))}
      </div>
      <div className='box-edit'>
        <Button name='edit' />
      </div>
    </div>
  )
}

export default Box
