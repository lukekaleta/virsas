import React from 'react'
import ReactDOM from 'react-dom'
import Root from './pages/root'
import MasterProvider from './providers/master.provider'

ReactDOM.render(
  <React.StrictMode>
    <MasterProvider>
      <Root />
    </MasterProvider>
  </React.StrictMode>,
  document.getElementById('root')
)
