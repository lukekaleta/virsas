import { useContext, useState } from 'react'
import { v4 as uuidv4 } from 'uuid'
import Box from '../../components/box'
import Button from '../../components/button'
import Dialog from '../../components/dialog'
import Header from '../../components/header'
import Tag from '../../components/tag'
import { DataContext } from '../../providers/data.provider'

import remote from '../../remote/data.json'

import './styles.scss'

const Root = () => {
  // state
  const [dialog, setDialog] = useState<boolean>(false)

  // context
  const { addItem, getData } = useContext(DataContext)

  // data
  const dataModel = {
    id: uuidv4(),
    leftItems: [],
    rightItems: [],
  }

  // dialog actions
  const handleDialogActions = () => {
    return setDialog(!dialog)
  }

  const handleAdd = () => {
    return addItem(dataModel)
  }

  return (
    <div>
      <div>
        <Header name='List' />
      </div>

      <div>
        <Box />
      </div>

      <div className='add-button '>
        <Button onClick={handleDialogActions} name='Add' />
      </div>

      {dialog && (
        <div>
          <Dialog clickAction={handleDialogActions} />
        </div>
      )}
    </div>
  )
}

export default Root
