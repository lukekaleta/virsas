import { ReactNode } from 'react'

import DataProvider from './data.provider'

type MasterProviderProps = {
  children: ReactNode
}

const MasterProvider = (props: MasterProviderProps) => {
  const { children } = props

  return <DataProvider>{children}</DataProvider>
}

export default MasterProvider
