import { createContext, ReactNode } from 'react'

type Context = {
  getData: any // TODO
  addItem: (value: any) => void
}

type DataProviderProps = {
  children: ReactNode
}

export const DataContext = createContext<Context>(null as any)

const DataProvider = (props: DataProviderProps) => {
  // props
  const { children } = props

  const getData = () => {
    return localStorage.getItem('list')
  }

  const addItem = (value: any) => {
    return localStorage.setItem('list', JSON.stringify(value))
  }

  return (
    <DataContext.Provider value={{ getData, addItem }}>
      {children}
    </DataContext.Provider>
  )
}

export default DataProvider
